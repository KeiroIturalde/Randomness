#!/bin/bash
release dhcpv6-pd interface eth2
delete dhcpv6-pd duid
renew dhcpv6-pd interface eth2
echo "IPv6 renewed!"